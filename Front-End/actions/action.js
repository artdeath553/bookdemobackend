import { actionTypes as actionResponse } from '../reducers/response'
import { actionTypes as actionLoading } from '../reducers/loadingStore'

export const getResp = (status, code, message, resStatus) => {
  return async (dispatch) => {
    if (message === 'Unauthorized') {
      dispatch(setLoadModal(true))
    } else {
      dispatch({ type: actionResponse.RESPONSE_GET,
        status: status,
        errorCode: code,
        message: message,
        resStatus: resStatus })
    }
  }
}

export const setLoadModal = (value) => {
    return async (dispatch, getState) => {
      dispatch({
        type: actionLoading.LOAD_MODAL_SET,
        key: 'modalAlert',
        value: value
      })
    }
  }