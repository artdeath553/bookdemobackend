import React, { Component } from 'react'
import { connect } from 'react-redux'
export class Breadcrumb extends Component {
  render() {
    return (
      <React.Fragment>
        <nav aria-label="breadcrumb" id='breadcrumb-style'>
          <ol className="breadcrumb">
            {this.props.children}
          </ol>
        </nav>
      </React.Fragment>
    )
  }
}

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = {
}

export default connect(mapStateToProps, mapDispatchToProps)(Breadcrumb)
