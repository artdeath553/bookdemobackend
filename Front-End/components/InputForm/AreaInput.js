import React, { Component } from 'react'
import classNames from 'classnames'

export default class AreaInput extends Component {
    render() {

        const { className, classLabel, meta, label, input,
            type, placeholder, name, disabled, id, row, required } = this.props

        const labelClass = classNames(
            { [`${classLabel}`]: !meta.touched },
            { [`${classLabel}`]: !meta.error && meta.touched },
            { ' text-danger ': meta.error && meta.touched && disabled !== true })

        const inputClass = classNames(className, {
            ' is-invalid ': meta.error && meta.touched && disabled !== true
        })

        return (
            <React.Fragment>
                <div className='form-group'>
                    <label className={labelClass} htmlFor='name'>{label} {required && <span className='text-danger'>*</span>}</label>
                    <textarea
                        {...input}
                        placeholder={placeholder}
                        className={inputClass}
                        disabled={disabled}
                        type={type}
                        name={name}
                        id={id}
                        rows={row}
                    />
                    {<p className='text-left' style={{ color: 'red', fontSize: '15px' }}>
                        {(this.props.meta.error && this.props.meta.touched && this.props.disabled !== true) && this.props.meta.error}
                    </p>}
                </div>
            </React.Fragment>
        )
    }
}
