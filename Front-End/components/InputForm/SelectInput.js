import React, { Component } from 'react'
import classNames from 'classnames'
import map from 'lodash/map'

export default class SelectInput extends Component {
    render() {

        const { className, classLabel, meta, label, input, 
            name, disabled, id, required, optionMap, defaultValue } = this.props

        const labelClass = classNames(
            { [`${classLabel}`]: !meta.touched },
            { [`${classLabel}`]: !meta.error && meta.touched },
            { ' text-danger ': meta.error && meta.touched && disabled !== true })

        const inputClass = classNames(className, {
            ' is-invalid ': meta.error && meta.touched && disabled !== true
        })

        return (
            <React.Fragment>
                <div className='form-group'>
                    <label className={labelClass} htmlFor='name'>{label} {required && <span className='text-danger'>*</span>}</label>
                    <select
                        {...input}
                        className={inputClass}
                        disabled={disabled}
                        name={name}
                        id={id} >
                        {defaultValue && <option defaultValue> {defaultValue} </option>}
                        {map(optionMap, (dataMap) => {
                            return <option value={dataMap.uid} key={dataMap.uid}> {dataMap.name} </option>
                        })}
                    </select>
                    {<p className='text-left' style={{ color: 'red', fontSize: '15px' }}>
                        {(this.props.meta.error && this.props.meta.touched && this.props.disabled !== true) && this.props.meta.error}
                    </p>}
                </div>
            </React.Fragment>
        )
    }
}
