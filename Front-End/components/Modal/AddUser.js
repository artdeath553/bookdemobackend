import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { Form, FormGroup, Label, Input, FormText } from "reactstrap";
import axios from 'axios'

export default function AddUser(props) {
const Url ="192.168.31.00"

  function Senddata(data) {
    console.log(data);
    axios.post(Url+"Register",data).then(res=>{
      console.log(res.data);
      props.toggle
  })
  }


  const [accountList, setAccountList] = useState([]);
  const [account, setaccount] = useState({
    level: "",
    imgprofile: "",
    username: "",
    password: "",
    fristname: "",
    lastname: "",
    address: "",
    phone: "",
    email: ""
  });
  const { buttonLabel, className } = props;
  const [modal, setModal] = useState(false);
  const toggle = () => setModal(!modal);
  return (
    <div>
      <Modal isOpen={props.isModal} toggle={props.toggle} className={className}>
        <ModalHeader toggle={props.toggle}>เพิ่มสมาชิก</ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="exampleSelect">ระดับผู้ใช้งาน</Label>
              <Input
                type="select"
                name="level"
                id="level"
                value={account.level}
                onChange={e => {
                  setaccount({ ...account, level: e.target.value });
                }}
              >
                <option>Admin</option>
                <option>User</option>
              </Input>
            </FormGroup>
            <FormGroup>
              <Label>เลือกรูปโปรไฟล์</Label>
              <Input
                type="file"
                name="imgprofile"
                id="imgprofile"
                value={account.imgprofile}
                onChange={e => {
                  setaccount({ ...account, imgprofile: e.target.value });
                }}
              />
            </FormGroup>
            <FormGroup>
              <Label>ผู้ใช้งาน</Label>
              <Input
                type="email"
                name="username"
                id="username"
                placeholder="username"
                value={account.username}
                onChange={e => {
                  setaccount({ ...account, username: e.target.value });
                }}
              />
            </FormGroup>
            <FormGroup>
              <Label>รหัสผ่าน</Label>
              <Input
                type="password"
                name="password"
                id="password"
                placeholder="password"
                value={account.password}
                onChange={e => {
                  setaccount({ ...account, password: e.target.value });
                }}
              />
            </FormGroup>
            <FormGroup>
              <Label>ชื่อจริง</Label>
              <Input
                type="text"
                name="fristname"
                id="fristname"
                placeholder="fristname"
                value={account.fristname}
                onChange={e => {
                  setaccount({ ...account, fristname: e.target.value });
                }}
              />
            </FormGroup>
            <FormGroup>
              <Label>นามสกุล</Label>
              <Input
                type="text"
                name="lastname"
                id="lastname"
                placeholder="lastname"
                value={account.lastname}
                onChange={e => {
                  setaccount({ ...account, lastname: e.target.value });
                }}
              />
            </FormGroup>
            <FormGroup>
              <Label>ที่อยู่</Label>
              <Input
                type="textarea"
                name="address"
                id="address"
                placeholder="address"
                value={account.address}
                onChange={e => {
                  setaccount({ ...account, address: e.target.value });
                }}
              />
            </FormGroup>
            <FormGroup>
              <Label>เบอร์โทรศัพท์</Label>
              <Input
                type="text"
                name="phone"
                id="phone"
                placeholder="phone"
                value={account.phone}
                onChange={e => {
                  setaccount({ ...account, phone: e.target.value });
                }}
              />
            </FormGroup>
            <FormGroup>
              <Label>อีเมลล์</Label>
              <Input
                type="email"
                name="email"
                id="email"
                placeholder="email"
                value={account.email}
                onChange={e => {
                  setaccount({ ...account, email: e.target.value });
                }}
              />
            </FormGroup>
            <Button
              color="primary"
              onClick={e => {
                e.preventDefault();
                setAccountList({ ...account, index: accountList.length });
                Senddata({ ...account, index: accountList.length });
              }}
            >
              ยืนยัน
            </Button>{" "}
            <Button color="secondary" onClick={props.toggle}>
              ยกเลิก
            </Button>
          </Form>
        </ModalBody>
        <ModalFooter></ModalFooter>
      </Modal>
    </div>
  );
}