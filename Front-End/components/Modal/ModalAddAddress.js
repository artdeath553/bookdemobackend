import React, { Component } from 'react'
import { TiWarningOutline, TiTick } from 'react-icons/ti'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'

export default class ModalAddAddress extends Component {

  render() {
    return (
      <Modal isOpen={this.props.modal} toggle={this.props.toggle} className='modal-lg modal-Add-Address'>
        <ModalBody>
          <div className='row'>
            <div className='col-12 text-left'>
              <span className='title-modal'> Add New Address </span>
            </div>
            <div className='col-12 col-lg-6 mt-lg-4 mt-5'>
              <div className='form-group'>
                <label className='text-muted' htmlFor='Firstname'>Firstname</label>
                <input type='text' className='form-control' id='Firstname' placeholder='Input' />
              </div>
            </div>
            <div className='col-12 col-lg-6 mt-lg-4'>
              <div className='form-group'>
                <label className='text-muted' htmlFor='Surname'>Surname</label>
                <input type='text' className='form-control' id='Surname' placeholder='Input' />
              </div>
            </div>
            <div className='col-12 mt-3'>
              <div className='form-group'>
                <label className='text-muted' htmlFor='Address'>Address</label>
                <input type='text' className='form-control' id='Address' placeholder='Input' />
              </div>
            </div>
            <div className='col-12 col-lg-6 mt-3'>
              <div className='form-group'>
                <label className='text-muted' htmlFor='Province'>Province</label>
                <input type='text' className='form-control' id='Province' placeholder='Input' />
              </div>
            </div>
            <div className='col-12 col-lg-6 mt-3'>
              <div className='form-group'>
                <label className='text-muted' htmlFor='City'>City</label>
                <input type='text' className='form-control' id='City' placeholder='Input' />
              </div>
            </div>
            <div className='col-12 col-lg-6 mt-3'>
              <div className='form-group'>
                <label className='text-muted' htmlFor='Country'>State/Country</label>
                <select id='Country' name='Country' className='form-control'>
                  <option defaultValue> Thailand </option>
                </select>
              </div>
            </div>
            <div className='col-4 mt-3'>
              <div className='form-group'>
                <label className='text-muted' htmlFor='Zipcode'>Zipcode</label>
                <input type='text' className='form-control' id='Zipcode' placeholder='' />
              </div>
            </div>
            <div className='col-12 col-lg-6 mt-3'>
              <div className='form-group'>
                <label className='text-muted' htmlFor='phone'>Phone Number</label>
                <input type='text' className='form-control' id='phone' placeholder='' />
              </div>
            </div>

            <div className='col-12 mt-3'>
              <hr />
            </div>

            <div className='col-12 mt-3 text-center text-lg-right'>
            <button type='button' className='btn-clean mr-4' onClick={this.props.Closetoggle}>Cancel</button>
            <button type='button' className='btn-citrus'>Save Address</button>
            </div>
          </div>
        </ModalBody>
      </Modal>
    )
  }
}
