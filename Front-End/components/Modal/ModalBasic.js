import React, { Component } from 'react'
import { TiWarningOutline, TiTick } from 'react-icons/ti'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'

export default class ModalBasic extends Component {
    _renderType = () => {
      switch (this.props.typeModal) {
        case 'waring':
          return <span className='text-yellow'> <TiWarningOutline className='icon-modal' /> WARNING </span>
        case 'error':
          return <span className='text-red'> <TiWarningOutline className='icon-modal' /> ERROR </span>
        case 'ok':
          return <span className='text-green'> <TiTick className='icon-modal' /> SUCCESS </span>
        default:
          return <span className=''> {this.props.title} </span>
      }
    }
    render() {
      return (
        <Modal isOpen={this.props.modal} toggle={this.props.toggle} className={this.props.className}>
          <ModalHeader toggle={this.props.Closetoggle} className=''> {this._renderType()}</ModalHeader>
          <ModalBody>
            {this.props.content}
          </ModalBody>
          <ModalFooter>
            <Button color='secondary' onClick={this.props.Closetoggle}>Close</Button>
          </ModalFooter>
        </Modal>
      )
    }
}
