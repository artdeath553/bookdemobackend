import React, { Component } from 'react'
import numeral from 'numeral'
import { Modal, ModalBody, ModalFooter } from 'reactstrap'

export default class ModalQR extends Component {
    render() {
        return (
            <Modal isOpen={this.props.isModal} toggle={this.props.toggleModal} className='modal-QR'>
                <ModalBody>
                    <div className='row'>
                        <div className='col-3 middle-box'>
                            <img src='/images/Logo_Test.png' className='image-logo' />
                        </div>
                        <div className='col-7 middle-left-box pl-0'>
                            <div className='row'>
                                <div className='col-12'>
                                    <span className='title-bis'> BIS MARKET </span><br />
                                    <span className='dest-bis'> Payment Secure with GBPrimePay </span>
                                </div>
                            </div>
                        </div>
                        <div className='col-2 middle-box'>
                            <span className='text-close' onClick={this.props.toggleModal}>X</span>
                        </div>
                    </div>

                    <div className='row'>
                        <div className='col-12 middle-box py-5'>
                            <img src='/images/QR_Code.png' className='image-qr' />
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter className='footer-QR middle-box py-4'>
                    <div className='row'>
                        <div className='col-12 text-center'>
                            <span className='dest-scan'> Scan QR Code to Pay your Order(s)</span> <br />
                            <span className='price-scan'> {numeral('400.00').format('0,0.00')} THB</span>
                        </div>
                    </div>
                </ModalFooter>
            </Modal>
        )
    }
}
