import React, { Component } from 'react'

export default class TravelPackage extends Component {
    render() {
        return (
            <div className='container' id='home-page'>
                <div className='row' id='sector-travel'>
                    <div className='col-12 text-center'>
                        <span className='title-sector'> {this.props.title} </span>
                    </div>

                    <div className='col-12 pb-5 text-center'>
                        <span className='topic-sector'> {this.props.topic} </span>
                    </div>

                    <div className='col-12 col-lg-4 text-center'>
                        <div className='row'>
                            <div className='col-12'>
                                <img src='/images/home/Rectangle_32.png' className='image-travel' />
                            </div>

                            <div className='col-12 name-travel'>
                                <span className='my-3'> ตัวอย่างแพ็กเกจท่องเที่ยว </span>
                            </div>

                            <div className='col-12 detail-travel'>
                                <span> รายละเอียดแพ็กเกจท่องเที่ยวสั้นๆ </span>
                            </div>
                        </div>
                    </div>

                    <div className='col-12 col-lg-4 text-center'>
                        <div className='row'>
                            <div className='col-12'>
                                <img src='/images/home/Rectangle_32.png' className='image-travel' />
                            </div>

                            <div className='col-12 name-travel'>
                                <span className='my-3'> ตัวอย่างแพ็กเกจท่องเที่ยว </span>
                            </div>

                            <div className='col-12 detail-travel'>
                                <span> รายละเอียดแพ็กเกจท่องเที่ยวสั้นๆ </span>
                            </div>
                        </div>
                    </div>

                    <div className='col-12 col-lg-4 text-center'>
                        <div className='row'>
                            <div className='col-12'>
                                <img src='/images/home/Rectangle_32.png' className='image-travel' />
                            </div>

                            <div className='col-12 name-travel'>
                                <span className='my-3'> ตัวอย่างแพ็กเกจท่องเที่ยว </span>
                            </div>

                            <div className='col-12 detail-travel'>
                                <span> รายละเอียดแพ็กเกจท่องเที่ยวสั้นๆ </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
