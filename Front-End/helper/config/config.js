import config from '../../configs'

let hostFront = config.hostFrontend
let server = config.hostBackend
let path_image = `${server}images/`

let localeStoreKey = {
  token: '@authToken:token',
  locale: '@authToken:locale'
}

export {
  server,
  localeStoreKey,
  hostFront,
  path_image
}
