import split from 'lodash/split'

export const splitUrl = (url, key) => {
    let pathname = split(url, key, 2)[1]
    return pathname
}