export function resizeImage(datas) {
    return new Promise(async function(resolve, reject) {
      const width = 500
      const height = 300
      const fileName = datas.name
      const reader = new FileReader()
      reader.readAsDataURL(datas)
      reader.onload = event => {
        const img = new Image()
        img.src = event.target.result
        img.onload = () => {
          const elem = document.createElement('canvas')
  
          var w = img.width
          var h = img.height
  
          if (w > h) {
            if (w > 720) {
              h *= 720 / w; w = 720
            }
          } else {
            if (h > 480) {
              w *= 480 / h; h = 480
            }
          }
  
          elem.width = w
          elem.height = h
          const ctx = elem.getContext('2d')
          // img.width and img.height will contain the original dimensions
          ctx.drawImage(img, 0, 0, w, h)
          ctx.canvas.toBlob((blob) => {
            const file = new File([blob], fileName, {
              type: 'image/jpeg',
              lastModified: Date.now()
            })
            resolve(file)
          }, 'image/jpeg', 1)
        }
        reader.onerror = error => console.log(error)
      }
    })
  }
  
export const imageBlobToBase64 = (blob) => {
    return new Promise((resolve, reject) => {
      const readerFile = new FileReader()
      readerFile.addEventListener(
        'load',
        () => {
          const base64Result = readerFile.result
          resolve(base64Result)
        },
        false
      )
      readerFile.readAsDataURL(blob)
    })
  }