export const isError = (reponse) => {
  if (reponse.status > 299 || reponse.status < 200) {
    return false
  } else {
    return true
  }
}
