import logger from 'redux-logger'
import {applyMiddleware, createStore} from 'redux'
import thunkMiddleware from 'redux-thunk'
import reducer from '../reducers/index'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'


const makeConfiguredStore = (reducer, initialState) => {
    if(process.env.NODE_ENV !== 'production'){
        const { composeWithDevTools } = require('redux-devtools-extension');
        let result = createStore(reducer, initialState, composeWithDevTools(applyMiddleware(...[thunkMiddleware, logger])))
        return result;
    } 

        return createStore(reducer, initialState, applyMiddleware(thunkMiddleware))
     

}

export const makeStore = (initialState, {isServer, req, debug, storeKey}) => {

    if (isServer) {
        

        initialState = initialState || {fromServer: 'foo'};

        return makeConfiguredStore(reducer, initialState);

    } else {

        const persistConfig = {
            key: 'bis-root',
            whitelist: ['cart'], // make sure it does not clash with server keys
            storage
        };

        const persistedReducer = persistReducer(persistConfig, reducer);
        const store = makeConfiguredStore(persistedReducer, initialState);

        store.__persistor = persistStore(store); // Nasty hack

        return store;
    }
};

export const setClientState = (clientState) => ({
    type: SET_CLIENT_STATE,
    payload: clientState
});