// Login.jsx
import React, { Component } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import Layout from "../components/layout";  

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }
  handleInputChange = event => {
    const { value, name } = event.target;
    this.setState({
      [name]: value
    });
  };
  onSubmit = event => {
    event.preventDefault();
    onSubmit = event => {
      event.preventDefault();
      fetch("/api/authenticate", {
        method: "POST",
        body: JSON.stringify(this.state),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => {
          if (res.status === 200) {
            this.props.history.push("/");
          } else {
            const error = new Error(res.error);
            throw error;
          }
        })
        .catch(err => {
          console.error(err);
          alert("Error logging in please try again");
        });
    };
  };
  render() {
    return (
      <Layout>
 <Form inline>
        <FormGroup>
          <Label for="exampleEmail" hidden>
            Username
          </Label>
          <Input
            type="email"
            name="email"
            placeholder="กรุณาใส่ Username "
            value={this.state.email}
            onChange={this.handleInputChange}
            required
          />
        </FormGroup>{" "}
        <FormGroup>
          <Label for="examplePassword" hidden>
            Password
          </Label>
          <Input
              type="password"
              name="password"
              placeholder="กรุณาใส่ Password "
              value={this.state.password}
              onChange={this.handleInputChange}
              required
          />
        </FormGroup>{" "}
        <Button>ยืนยัน</Button>
      </Form>
      </Layout>
     
    );
  }
}
