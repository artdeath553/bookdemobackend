import React from "react";
import {Provider} from "react-redux";
import App from "next/app";
import withRedux from "next-redux-wrapper";
import {makeStore} from "../lib/redux";
import {PersistGate} from 'redux-persist/integration/react';
import '../styles/index.scss';

export default withRedux(makeStore, {debug: true})(class MyApp extends App {

    render() {
        const {Component, pageProps, store} = this.props;
        return (
          <Provider store={store}>
              <PersistGate persistor={store.__persistor} loading={<div>Loading</div>}>
                  <Component {...pageProps} />
              </PersistGate>
          </Provider>

        );
    }

});