import React, { useState } from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { connect } from "react-redux";
import { setClientState } from "../lib/redux";
import Layout from "../components/layout";
import { Table } from "reactstrap";
import { Button } from "reactstrap";
import { Container, Row, Col } from "reactstrap";
import axios from "axios";
import AddUser from "../components/Modal/AddUser";
import EditUser from "../components/Modal/EditUser";
import DeleteUser from "../components/Modal/DeleteUser";
function createData(id, FirstName, LastName, Username) {
  return { id, FirstName, LastName, Username };
}

const rows = [
  createData(1, "assssssaa", "bbb", "test1"),
  createData(2, "aaa", "bbb", "test2"),
  createData(3, "aaa", "bbb", "test3"),
  createData(4, "aaa", "bbb", "test4"),
  createData(5, "aaa", "bbb", "test5")
];
// const Url = ""
// const [row, setRow] = useState([])
var Data = [];
class contact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModal: false,
      isModalEdit: false,
      isModalDelete: false
    };
  }

  onToggle = key => {
    this.setState(prevState => ({
      [key]: !prevState[key]
    }));
  };

  componentDidMount = () => {
    
    // this.loadData();
  };

  // loadData = () => {
  //   axios.get(Url+"GetListUser").then(res=>{
  //       console.log(res);
  //       setRow(res)
  //   })
  // }
  render() {
    return (
      <Layout>
        <div id="contact-page-bg">
          <div className="container">
            <h1> รายการสมาชิก </h1>
            <Button color="success" onClick={() => this.onToggle("isModal")}>
              เพิ่มสมาชิก
            </Button>
            <AddUser
              isModal={this.state.isModal}
              toggle={() => this.onToggle("isModal")}
            />
            &nbsp;
            <Table bordered hover>
              <thead>
                <tr>
                  <th align="center">#</th>
                  <th align="center">First Name</th>
                  <th align="center">Last Name</th>
                  <th align="center">Username</th>
                  <th align="center">Action</th>
                </tr>
              </thead>
              <tbody>
                {rows.map(row => (
                  <tr key={row.id}>
                    <th scope="row">{row.id}</th>
                    <td align="center">{row.FirstName}</td>
                    <td align="center">{row.LastName}</td>
                    <td align="center">{row.Username}</td>
                    <td align="center">
                      <Button
                        color="info"
                        onClick={() => {
                          this.onToggle("isModalEdit");
                          Data = row;
                        }}
                      >
                        Edit
                      </Button>{" "}
                      <Button
                        color="danger"
                        onClick={() => {
                          this.onToggle("isModalDelete");
                          Data = row;
                        }}
                      >
                        Delete
                      </Button>{" "}
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <EditUser
              isModal={this.state.isModalEdit}
              toggle={() => {
                this.onToggle("isModalEdit");
              }}
              data={Data}
            />
            <DeleteUser
              isModal={this.state.isModalDelete}
              toggle={() => {
                this.onToggle("isModalDelete");
              }}
              data={Data.id}
            />
          </div>
        </div>
      </Layout>
    );
  }
}

export default connect(state => state, { setClientState })(contact);
