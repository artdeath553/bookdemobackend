import React, { useState } from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import {  Form, FormGroup, Label, Input } from 'reactstrap';
import { connect } from "react-redux";
import { setClientState } from "../lib/redux";
import Layout from "../components/layout";
import { Table } from "reactstrap";
import { Button } from "reactstrap";
import { Container, Row, Col } from "reactstrap";
import axios from "axios";
import AddBook from "../components/Modal/AddBook";
import EditBook from "../components/Modal/EditBook";
import DeleteBook from "../components/Modal/DeleteBook";

const Url = ""
// const [row, setRow] = useState([])

function createData(id, FirstName, LastName, Username) {
  return { id, FirstName, LastName, Username };
}

const rows = [
  createData(1, "images/1000227435_front_XXL.jpg", "bbb", "test1"),
  createData(2, "images/1000225670_front_XXL.jpg", "bbb", "test2"),
  createData(3, "images/1000227435_front_XXL.jpg", "bbb", "test3"),
  createData(4, "images/1000227435_front_XXL.jpg", "bbb", "test4"),
  createData(5, "images/1000227435_front_XXL.jpg", "bbb", "test5")
];

var Data = [];
class masterBook extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModal: false,
      isModalEdit: false,
      isModalDelete: false
    };
  }

  onToggle = key => {
    this.setState(prevState => ({
      [key]: !prevState[key]
    }));
  };

  componentDidMount = () => {
    this.loadData();
  };

  loadData = () => {
    // axios.get(Url+"GetListBook").then(res=>{
    //     console.log(res);
    //     setRow(res)
    // })
  //   fetch('https://192.168.1.46:8080/demo7')
  // .then(response => response.json())
  // .then(json => console.log("ข้อมูล คือ ",json))
  }
  render() {
    return (
      <Layout>
        <div id="contact-page-bg">
          <div className="container">
            <h1>รายการหนังสือ</h1>
            <Row>
              <Col xs="6" sm="4">
                <Button
                  color="success"
                  onClick={() => this.onToggle("isModal")}
                >
                  เพิ่มหนังสือ
                </Button>
                <AddBook
                  isModal={this.state.isModal}
                  toggle={() => this.onToggle("isModal")}
                />
              </Col>
              <Col xs="6" sm="4"></Col>
              <Col sm="4" style={{float:"right"}}>
                <Form inline>
                  <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                    <Input
                      type="text"
                      name="search"
                      id="search"
                      placeholder="ค้นหา"
                    />
                  </FormGroup>
                  <Button>ค้นหา</Button>
                </Form>
              </Col>
            </Row>
            &nbsp;
            <Table bordered hover>
              <thead>
                <tr>
                  <th align="center">#</th>
                  <th align="center">ภาพหนังสือ</th>
                  <th align="center">ชื่อหนังสือ</th>
                  <th align="center">จำนวนหนังสือทั้งหมด</th>
                  <th align="center">จำนวนหนังสือคงเหลือ</th>
                  <th align="center">สถานะการยืม</th>
                  <th align="center">Action</th>
                </tr>
              </thead>
              <tbody>
                {rows.map(row => (
                  <tr key={row.id}>
                    <th scope="row">{row.id}</th>
                    <td align="center">
                      <img src={row.FirstName} height={200} />
                    </td>
                    <td align="center">{row.LastName}</td>
                    <td align="center">{row.Username}</td>
                    <td align="center">{row.Username}</td>
                    <td align="center">{row.Username}</td>
                    <td align="center">
                      <Button
                        color="info"
                        onClick={() => {
                          this.onToggle("isModalEdit");
                          Data = row;
                        }}
                      >
                        แก้ไข
                      </Button>{" "}
                      <Button
                        color="danger"
                        onClick={() => {
                          this.onToggle("isModalDelete");
                          Data = row;
                        }}
                      >
                        ลบ
                      </Button>{" "}
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <EditBook
              isModal={this.state.isModalEdit}
              toggle={() => {
                this.onToggle("isModalEdit");
              }}
              data={Data}
            />
            <DeleteBook
              isModal={this.state.isModalDelete}
              toggle={() => {
                this.onToggle("isModalDelete");
              }}
              data={Data.id}
            />
          </div>
        </div>
      </Layout>
    );
  }
}

export default connect(state => state, { setClientState })(masterBook);
