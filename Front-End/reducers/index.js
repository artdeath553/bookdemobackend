import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

import test from './test'
import sidebar from './sidebar'
import response from './response'

export const appReducer = combineReducers({
  form: formReducer,
  response,
  sidebar,
  test,
})

const rootReducer = (state, action) => {
  if (action.type === 'ACCOUNT/SIGNOUT') {
    state = undefined
  }

  return appReducer(state, action)
}
export default rootReducer
