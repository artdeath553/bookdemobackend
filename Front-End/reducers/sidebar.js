export const actionTypes = {
  SIDEBAR_ON: 'SIDEBAR/TOGGLE_ON',
  SIDEBAR_OFF: 'SIDEBAR/TOGGLE_OFF'
}

const initialState = {
  toggleSidebar: false
};

const sidebar = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SIDEBAR_ON: {
      return {
        ...state,
        toggleSidebar: true
      }
    }
    case actionTypes.SIDEBAR_OFF: {
      return {
        ...state,
        toggleSidebar: false
      }
    }

    default: {
      return state
    }
  }
}

export default sidebar